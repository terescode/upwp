ARG PHP_VERSION=7.4

################
# php-fpm

FROM php:${PHP_VERSION}-fpm

RUN apt-get update \
  && apt-get install -y \
    libfreetype6-dev \
    libjpeg-dev \
    libmagickwand-dev \
    libpng-dev \
    libzip-dev \
    git \
    openssh-client \
    zip \
    unzip \
    procps \
    vim \
    default-mysql-client \
    nginx \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN LT74=$(php -r "if (version_compare('${PHP_VERSION}', '7.4', '<')) { echo 'true'; } else { echo 'false'; } ") \
  && if [ "$LT74" = "true" ]; then \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/; \
  else \
    docker-php-ext-configure gd --with-freetype --with-jpeg; \
  fi \
  && pecl install imagick \
  && rm -rf /tmp/pear \
  && docker-php-ext-install bcmath exif zip gd pdo pdo_mysql mysqli \
  && docker-php-ext-enable bcmath exif zip gd pdo pdo_mysql mysqli imagick \
  && rm -rf /etc/nginx/sites-enabled/default \
  && curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
  && chmod +x wp-cli.phar \
  && mv wp-cli.phar /usr/local/bin/wp

################
# Copy files

ADD --chown=www-data:www-data docker/site.conf /etc/nginx/sites-enabled/
ADD --chown=www-data:www-data docker/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

################
# Set working directory and expose ports

WORKDIR /var/www/html
EXPOSE 80
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]