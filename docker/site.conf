    client_max_body_size 100M;

	map $http_x_forwarded_proto $resolved_scheme {
        default "http";
        "https" "https";
    }

	map $resolved_scheme $fastcgi_https {
		default '';
		https on;
	}

    upstream php {
        server 127.0.0.1:9000;
    }

    server {
        listen 80;
	    listen [::]:80;
      
        root /var/www/html/app/public;

        error_log /var/log/nginx/error.log;
        access_log /var/log/nginx/access.log;

        index index.php;

        # Restrictions from local nginx/includes/restrictions.conf.hbs
        # Global restrictions configuration file.
        # Designed to be included in any server {} block.
        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        # Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
        # Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
        location ~ /\. {
            deny all;
            return 404;
        }

        # Deny access to any files with a .php extension in the uploads directory
        # Works in sub-directory installs and also in multisite network
        # Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
        location ~* /(?:uploads)/.*\.php$ {
            deny all;
        }

        # Gzip settings from local nginx/includes/gzip.conf.hbs
        gzip              on;
        gzip_comp_level   5;
        gzip_min_length   256;
        gzip_proxied      any;
        gzip_vary         on;

        gzip_types
            application/atom+xml
            application/javascript
            application/json
            application/ld+json
            application/manifest+json
            application/rss+xml
            application/vnd.geo+json
            application/vnd.ms-fontobject
            application/x-font-ttf
            application/x-web-app-manifest+json
            application/xhtml+xml
            application/xml
            font/opentype
            image/bmp
            image/svg+xml
            image/x-icon
            text/cache-manifest
            text/css
            text/plain
            text/vcard
            text/vnd.rim.location.xloc
            text/vtt
            text/x-component
            text/x-cross-domain-policy;

        # From local nginx/includes/wordpress-single.conf.hbs
        # Add trailing slash to */wp-admin requests.
        rewrite /wp-admin$ $resolved_scheme://$host$uri/ permanent;

        # From local nginx/site.conf.hbs
        
        #
        # Forward 404's to WordPress
        #
        error_page 404 = @wperror;
        location @wperror {
            rewrite ^/(.*)$ /index.php?q=$1 last;
        }

        #
        # Static file rules
        #
        location ~* \.(?:css|js)$ {
            access_log        off;
            log_not_found     off;
            add_header        Cache-Control "no-cache, public, must-revalidate, proxy-revalidate";
        }

        location ~* \.(?:jpg|jpeg|gif|png|ico|xml)$ {
            access_log        off;
            log_not_found     off;
            expires           5m;
            add_header        Cache-Control "public";
        }

        location ~* \.(?:eot|woff|woff2|ttf|svg|otf) {
            access_log        off;
            log_not_found     off;

            expires           5m;
            add_header        Cache-Control "public";

            # allow CORS requests
            add_header        Access-Control-Allow-Origin *;
        }

        location / {
            # This is cool because no php is touched for static content.
            # include the "?$args" part so non-default permalinks doesn't break when using query string
            try_files $uri $uri/ /index.php?$args;
        }

        location ~ ^/.+\.php(/|$) {
            try_files $uri =404;

            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include fastcgi_params;

            fastcgi_param   QUERY_STRING            $query_string;
            fastcgi_param   REQUEST_METHOD          $request_method;
            fastcgi_param   CONTENT_TYPE            $content_type;
            fastcgi_param   CONTENT_LENGTH          $content_length;

            fastcgi_param   SCRIPT_FILENAME         $document_root$fastcgi_script_name;
            fastcgi_param   SCRIPT_NAME             $fastcgi_script_name;
            fastcgi_param   PATH_INFO               $fastcgi_path_info;
            fastcgi_param   PATH_TRANSLATED         $document_root$fastcgi_path_info;
            fastcgi_param   REQUEST_URI             $request_uri;
            fastcgi_param   DOCUMENT_URI            $document_uri;
            fastcgi_param   DOCUMENT_ROOT           $document_root;
            fastcgi_param   SERVER_PROTOCOL         $server_protocol;

            fastcgi_param   GATEWAY_INTERFACE       CGI/1.1;
            fastcgi_param   SERVER_SOFTWARE         nginx/$nginx_version;

            fastcgi_param   REMOTE_ADDR             $remote_addr;
            fastcgi_param   REMOTE_PORT             $remote_port;
            fastcgi_param   SERVER_ADDR             $server_addr;
            fastcgi_param   SERVER_PORT             $server_port;
            fastcgi_param   SERVER_NAME             $host;

            fastcgi_param   REDIRECT_STATUS         200;

            fastcgi_index index.php;
            fastcgi_intercept_errors on;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

            fastcgi_pass php;
            fastcgi_param HTTPS off;
        }
    }